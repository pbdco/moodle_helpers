#!/bin/bash
# Script crea client en Keycloak y lo configura como issuers en Moodle

CLIENTID=$1
SECRET=$2

if [[ $1 == "AUTO" ]]; then
    CLIENTID=$(cat ./../../config.php | grep "wwwroot" | grep -Po "(?<=//)[^.'].*(?=\')")
fi


function getToken {

    curl -s \
        --location \
        --request POST "https://keycloak.entornos.net/auth/realms/master/protocol/openid-connect/token" \
        --header "Content-Type: application/x-www-form-urlencoded" \
        --data-urlencode "grant_type=client_credentials" \
        --data-urlencode "client_id=admin-cli" \
        --data-urlencode "client_secret=$SECRET"  |
    sed 's/.*access_token":"//g' | sed 's/".*//g'

}

    TOKEN=$(getToken)

function postClient {
    curl -s \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer ${TOKEN}" \
        -d '{

            "clientId": "'${CLIENTID}'",
            "surrogateAuthRequired": false,
            "enabled": true,
            "alwaysDisplayInConsole": false,
            "clientAuthenticatorType": "client-secret",
            "redirectUris": ["https://'${CLIENTID}'/*"],
            "webOrigins": [],
            "notBefore": 0,
            "bearerOnly": false,
            "consentRequired": false,
            "standardFlowEnabled": true,
            "implicitFlowEnabled": false,
            "directAccessGrantsEnabled": false,
            "serviceAccountsEnabled": true,
            "publicClient": false,
            "frontchannelLogout": false,
            "protocol": "openid-connect",
            "attributes": {},
            "authenticationFlowBindingOverrides": {},
            "fullScopeAllowed": true,
            "nodeReRegistrationTimeout": -1,
            "defaultClientScopes": ["profile","email"],
            "optionalClientScopes": [],
            "access": {"view":true,"configure":true,"manage":true}
        }' \
        https://keycloak.entornos.net/auth/admin/realms/master/clients
}

postClient


function getClientId {
    curl -s \
        -H "Authorization: Bearer ${TOKEN}" \
        https://keycloak.entornos.net/auth/admin/realms/master/clients?clientId=${CLIENTID} |

    sed 's/\",.*$//' | sed 's/\[{\"id\":\"//'
}

    echo "Client ID: $(getClientId)"

function getClientSecret {
    curl -s \
        -H "Authorization: Bearer ${TOKEN}" \
        https://keycloak.entornos.net/auth/admin/realms/master/clients/${1}/client-secret |
    sed 's/\"}.*$//' | sed 's/{\"type\":\"secret\",\"value\":\"//'
}

CLIENTSECRET="$(getClientSecret $(getClientId))"
    echo "Client Secret: $CLIENTSECRET"

# Crea client en Moodle
    echo "Executing oauth2_setup.php.."
    php ./oauth2_setup.php  --name='eeauth' --image='https://design.jboss.org/keycloak/logo/images/keycloak_icon_32px.png' --baseurl='https://keycloak.entornos.net/auth/realms/master'  --clientsecret=''$CLIENTSECRET'' --clientid=''$CLIENTID'' --showonloginpage='0' --enabled


# Activa Oauth2 como available authentication plugin: /admin/settings.php?section=manageauths

    if [[ $3 == "ENABLE" ]]; then
        echo "Run cfg.php to enable oauth2"
        #Obtiene cuales son los medios auth actuales
        
             ENABLEDPLUGINS=$(php ./cfg.php --name=auth)
                
                # Si no tiene el "oauth2", lo agrega
                if [[ $ENABLEDPLUGINS != *"oauth2"* ]]; then
                    php ./cfg.php --name=auth --set=$ENABLEDPLUGINS,oauth2
                fi

    fi


# Muestra listado de enabled auth plugins

    echo "Enabled auth plugins:"
    php ./cfg.php --name=auth

# Muestra listado de clients activos
    php ./oauth2_setup.php --list

exit 0
