#!/bin/bash -x

for x in $(docker ps --filter expose=80 --format='{{.Names}}'); do

  docker exec --user www-data $x  php /var/www/html/admin/cli/cfg.php --name=disableupdatenotifications --set=1

done
