<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script allows you to reset any local user password.
 *
 * @package    core
 * @subpackage cli
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions

// Define the input options.
$longparams = array(
        'help'                => false,
        'list'                => false,
        'by-name'             => false,
        'id'                  => false,
        'name'                => false,
        'image'               => false,
        'baseurl'             => false,
        'clientid'            => false,
        'clientsecret'        => false,
        'enabled'             => false,
        'showonloginpage'     => false,
        'basicauth'           => false,
        'sortorder'           => '0',
        'requireconfirmation' => false
);

// now get cli options
list($options, $unrecognized) = cli_get_params( $longparams, array(
    'l' => 'list',
    'h' => 'help'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

function help() {
$help = "Changes or creates oauth2 issuers from CLI.

Options:
  -h, --help                 Print out this help.
  -l, --list                 Print all issuers already configured.
                             If not set, will create a new issuer.
  --id                       Issuer ID. Ony used when edit an existing issuer.
                             If not set, will create a new issue if issuer with
                             name dont exists.
  --by-name                  Edit issuer based on its name instead of ID.
  --name                     Issuer name.
  --image                    Issuer logo URL. Required
  --baseurl                  Issuer base URL. This base URL will be used to
                             try OAuth2 discovery and autoconfiguring OAuth2
                             endpoints. This means appending:
                               .well-known/openid-configuration.
  --clientid                 OAuth2 clientID.
  --clientsecret             OAuth2 clientSecret.
  --enabled                  if this option is set, issuer will be enabled.
                             If not, disabled.
  --showonloginpage          if set this issuer will be shown on login page.
  --sortorder                Sort order when using multiple issuers.
  --requireconfirmation      If set will require confirmation.

";
  cli_write($help);
}

function print_issuer($issuer) {
  $format="%-19.19s| %s";
  cli_separator();
  cli_writeln(sprintf($format, 'Id', $issuer->get('id')));
  cli_separator();
  cli_writeln(sprintf($format, 'Name', $issuer->get('name')));
  cli_writeln(sprintf($format, 'Base URL', $issuer->get('baseurl')));
  cli_writeln(sprintf($format, 'Image', $issuer->get('image')));
  cli_writeln(sprintf($format, 'ClientID', $issuer->get('clientid')));
  cli_writeln(sprintf($format, 'Secret', $issuer->get('clientsecret')));
  cli_writeln(sprintf($format, 'Enabled', $issuer->get('enabled') ? 'Y' : 'N'));
  cli_writeln(sprintf($format, 'Login page', $issuer->get('showonloginpage') ? 'Y' : 'N'));
  cli_writeln(sprintf($format, 'Require confirm', $issuer->get('requireconfirmation') ? 'Y' : 'N'));
  cli_writeln(sprintf($format, 'Endpoints', ''));
  foreach(core\oauth2\api::get_endpoints($issuer) as $endpoint) {
    cli_writeln(sprintf($format, " + ".str_replace('_endpoint','', $endpoint->get('name')), $endpoint->get('url')));
  }
}

function update_issuer($data, $boolean_fields) {
  $issuer = core\oauth2\issuer::get_record([ 'id' => $data['id']]);
  if (!$issuer) cli_error('Cant update issuer that doesnt exists');
  $need_update = false;
  foreach($data as $k => $v) {
    if ( in_array($k, $boolean_fields)) $need_update = $need_update || boolval($issuer->get($k)) != boolval($v);
      else $need_update = $need_update || strcmp($v, $issuer->get($k));
  }
  foreach (core\oauth2\endpoint::get_records(['issuerid' => $issuer->get('id')]) as $endpoint) {
    $need_update = $need_update || strpos($endpoint->get('url'),$data['baseurl'])!=0;
  }

  if (!$need_update) {
    cli_writeln('OAuth2 not modified. Values are correct');
    return;
  }
  $endpoint = core\oauth2\endpoint::get_record([
    'issuerid' => $issuer->get('id'),
    'name' => 'discovery_endpoint'
  ]);
  if ($endpoint) $endpoint->delete();
  core\oauth2\api::update_issuer((object) $data);
}

if ($options['help']) {
  help();
  die();
}

cron_setup_user();

if (! $options['list'] &&
    ! $options['by-name']  &&
    ! $options['id']  &&
    ! $options['name']
) {
  help();
  cli_error('Must set list mode, get-id-by-name, or id / name to edit or create an issuer');
}else {
  if ( $options['by-name'] ) {
    $issuer = core\oauth2\issuer::get_record([ 'name' => $options['by-name']]);
    if (!$issuer) {
      cli_error("Issuer with name ${option['by-name']} not found!");
    }
    $options['id'] = $issuer->get('id');
  }
  if (! $options['list'] && !$options['image']) {
    help();
    cli_error('Image required');
  }
  if (! $options['list'] && !$options['baseurl']) {
    help();
    cli_error('BaseURL required');
  }
  if (! $options['list'] && ( !$options['clientid'] || !$options['clientsecret'])) {
    help();
    cli_error('ClientID and SecretID');
  }
}


if ($options['list']){
  foreach( core\oauth2\api::get_all_issuers() as $issuer) print_issuer($issuer);
  return;
}

if ($options['baseurl']){
  $options['baseurl'] = rtrim($options['baseurl'],'/');
}
$data = [];
$fields = [
  'id',
  'name',
  'image',
  'baseurl',
  'clientid',
  'clientsecret',
  'sortorder',
];
$boolean_fields = [
  'enabled',
  'showonloginpage',
  'basicauth',
  'requireconfirmation',
];

foreach($fields as $key){
  if ( $options[$key] )$data[$key] = $options[$key];
}
foreach($boolean_fields as $key){
  $data[$key] = $options[$key];
}

if ( $options['id'] ) update_issuer($data, $boolean_fields);
else{
  $issuer = core\oauth2\issuer::get_record([ 'name' => $options['name']]);
  if (!$issuer) core\oauth2\api::create_issuer((object) $data);
  else {
    $data['id'] = $issuer->get('id');
    update_issuer($data, $boolean_fields);
  }
}