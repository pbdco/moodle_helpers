El script keycloak ee-auto tiene 3 argumentos:
		○ 1=AUTO: Si está en auto, busca el wwwroot en el config.php. Sino, se puede pasar manualmente el dominio
		○ 2=Secret de la master de keycloak.
        ○ 3=ENABLE ejecuta el cfg.php para setear el oauth2 para habilitarlo. Si no se define, no cambia nada (el plugin puede quedar desactivado).
